// Теоретичні питання:
// 1. Як можна оголосити змінну у Javascript?
// Оголосити змінну можна за допомогою слів let і const (раніше була можливість використовувати var,
// але це вважається застарілим способом, виключенням може бути підтримка старих веб-проєктів).
// У змінну const вносить дані один раз, які по суті є константою, в свою чергу let дозволяє постійно вносити і
// змінювати дані змінної.

// 2. У чому різниця між функцією prompt та функцією confirm?
// Різниця між prompt і confirm в тому що prompt вводить повідомлення та чекає, поки користувач введе текст, а потім
// повертає введене значення або null, якщо введення скасовано, а confirm виводить повідомлення та чекає, доки
// користувач натисне OK або CANCEL і повертає true/false.

// 3. Що таке неявне перетворення типів? Наведіть один приклад.
// Неявне перетворення - це коли значення можуть бути конвертовані між різними типами даних автоматично.
// 120 + "10" = "12010"

// Завдання:
// 1. Оголосіть дві змінні: admin та name. Встановіть ваше ім'я в якості значення змінної name. Скопіюйте це значення в
// змінну admin і виведіть його в консоль.

let name = "Alex";
admin = name;
console.log(admin);

// 2. Оголосити змінну days і ініціалізувати її числом від 1 до 10. Перетворіть це число на кількість секунд і
// виведіть на консоль.

let days = 7;
console.log(days * 86400);

// 3. Запитайте у користувача якесь значення і виведіть його в консоль.

let userQuiz = prompt ("Your nails colour:");
console.log(userQuiz);